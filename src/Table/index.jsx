import { AppstoreOutlined, FilterOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Checkbox, Dropdown, Input, Menu, Popover, Row, Space, Table } from 'antd';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import birebirIco from './birebir.png';
import gizleIco from './gizle.png';
import icerenIco from './iceren.png';
import ortalamaIco from './ortalama.png';
import siralamaIco from './siralama.png';
import './tablo.scss';
import excelIco from './toExcel.png';
import toplamIco from './toplam.png';

export default function Tablo(props) {
  const { dataSource, columns, title, extra, className, summary, pagination, qFilter, rowClassName, ref } = props;

  const [filtData, setfiltData] = useState(dataSource);
  const [kolonSıralama, setKolonSıralama] = useState(false);
  const [aramaTuru, setAramaTuru] = useState('iceren');
  const [filtColumns, setFiltColumns] = useState([]);
  const [pageSize, setPageSize] = useState(pagination && pagination.pageSize);
  const tableId = Math.random();


  const kolonOzellikleri = (tmpFiltColumns) =>
    tmpFiltColumns.map((item) => {
      const col = item;
      if (col.type === 'tarih') {
        col.render = (value) => {
          const tarih = value ? moment(value) : moment(null);
          return tarih.isValid() ? tarih.format('DD.MM.YYYY hh:mm:ss') : undefined;
        };
        col.width = 180;
      }

      col.onHeaderCell = () => ({
        draggable: kolonSıralama,
        onDragStart: (e) => e.dataTransfer.setData('colName', col.dataIndex),
        onDragOver: (e) => e.preventDefault(),
        onDrop: (e) => {
          // eslint-disable-next-line no-unused-expressions
          kolonSıralama &&
            setFiltColumns((cols) => {
              const kaynakColAdı = e.dataTransfer.getData('colName');
              const kaynakIndex = cols.map((x) => x.dataIndex).indexOf(kaynakColAdı);
              const hedefIndex = cols.map((x) => x.dataIndex).indexOf(col.dataIndex);
              const tmpCol = cols[kaynakIndex];
              // sil
              const tmpCols = cols.filter((x, i) => i !== kaynakIndex);
              // ekle
              tmpCols.splice(hedefIndex, 0, tmpCol);
              return tmpCols;
            });
        },
        style: kolonSıralama && {
          animation: `swell 7s ease ${Math.random()}s infinite`,
          transform: 'translate3d(0, 0, 0) !important',
        },
        // onClick: () => {
        //   console.log(kolonSıralama);
        // },
      });

      col.sorter = (a, b) => {
        const colName = col.dataIndex;
        if (a[colName] && a[colName] !== null && b[colName] !== null) {
          const kars = a[colName].toString().localeCompare(b[colName].toString());
          return kars;
        }
        return a[colName] === null ? 1 : -1;
      };
      return col;
    });

  const kolonGizleme = () => {
    const options = columns.map((x) => x.title);
    const value = filtColumns.filter((col) => !col.hidden).map((col) => col.title);
    return (
      <div className="popupGorunum">
        <Checkbox
          onChange={(e) => {
            setFiltColumns((cols) => cols.map((x) => ({ ...x, hidden: !e.target.checked })));
          }}
        >
          Tümünü Seç
        </Checkbox>
        <Checkbox.Group
          value={value}
          onChange={(e) => {
            setFiltColumns((cols) => cols.map((x) => ({ ...x, hidden: e.indexOf(x.title) === -1 })));
          }}
          options={options}
        />
      </div>
    );
  };

  const altlıkHazırla = () => {
    if (summary) {
      return summary;
    }
    let altlıkGoster = 0;
    const tmpToplam = {};
    const tmpOrtalama = {};
    const aktifCols = filtColumns.filter((col) => !col.hidden && col.type !== 'tarih');
    aktifCols.forEach((col) => {
      const { dataIndex, toplam, ortalama } = col;
      if (toplam) {
        altlıkGoster += 1;
        tmpToplam[dataIndex] = 0;
        filtData.forEach((row) => {
          tmpToplam[dataIndex] += Number(row[dataIndex]) || 0;
        });
      }
      if (ortalama) {
        altlıkGoster += 1;
        tmpOrtalama[dataIndex] = { toplam: 0, adet: 0 };
        filtData.forEach((row) => {
          const sayı = Number(row[dataIndex]) || 0;
          if (sayı > 0) {
            tmpOrtalama[dataIndex].toplam += sayı;
            tmpOrtalama[dataIndex].adet += 1;
          }
        });
        if (tmpOrtalama[dataIndex].adet !== 0) {
          tmpOrtalama[dataIndex] = tmpOrtalama[dataIndex].toplam / tmpOrtalama[dataIndex].adet;
        } else {
          tmpOrtalama[dataIndex] = 0;
        }
      }
    });
    if (altlıkGoster !== 0) {
      return (
        <Table.Summary fixed>
          {Object.keys(tmpToplam).length > 0 && (
            <Table.Summary.Row>
              {aktifCols.map((col, i) => (
                <Table.Summary.Cell key={`toplam${i.title}`} index={i}>
                  {tmpToplam[col.dataIndex] ? <strong>Toplam: {tmpToplam[col.dataIndex].toLocaleString()}</strong> : null}
                </Table.Summary.Cell>
              ))}
            </Table.Summary.Row>
          )}
          {Object.keys(tmpOrtalama).length > 0 && (
            <Table.Summary.Row>
              {aktifCols.map((col, i) => (
                <Table.Summary.Cell key={`ortalama${i.title}`} index={i}>
                  {tmpOrtalama[col.dataIndex] ? <strong>Ort: {tmpOrtalama[col.dataIndex].toLocaleString()}</strong> : null}
                </Table.Summary.Cell>
              ))}
            </Table.Summary.Row>
          )}
        </Table.Summary>
      );
    }
    return null;
  };

  const olumluPropList = (propName) => {
    const options = columns.map((x) => x.title);
    const value = filtColumns.filter((col) => !col.hidden && col[propName] === true).map((col) => col.title);
    return (
      <div className="popupGorunum">
        <Checkbox
          onChange={(e) => {
            setFiltColumns((cols) => cols.map((x) => ({ ...x, [propName]: e.target.checked })));
          }}
        >
          Tümünü Seç
        </Checkbox>
        <Checkbox.Group
          value={value}
          onChange={(e) => {
            setFiltColumns((cols) => cols.map((x) => ({ ...x, [propName]: e.indexOf(x.title) !== -1 })));
          }}
          options={options}
        />
      </div>
    );
  };

  const tableToExcel = (SheetName) => {
    // Define your style class template.
    const excelStyle = '<style>.green { background-color: green; }</style>';
    const uri = 'data:application/vnd.ms-excel;base64,';
    // eslint-disable-next-line max-len
    const template = `<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->${excelStyle}<meta http=equiv="content-type" content="text-plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>`;
    const base64 = (s) => window.btoa(unescape(encodeURIComponent(s)));
    const format = (s, c) => s.replace(/{(\w+)}/g, (m, p) => c[p]);
    const antdtable = document.getElementById(tableId);
    const ctx = { worksheet: SheetName || 'Worksheet', table: antdtable.innerHTML };
    window.location.href = uri + base64(format(template, ctx));
  };

  const menu = () => (
    <Menu className="tableMenu">
      <Menu.Item
        className="tableMenuItem"
        key="kolonSıralama"
        icon={<img src={siralamaIco} className="menuIcon" alt="Sıralama" title="Sıralama" />}
        onClick={() => setKolonSıralama(!kolonSıralama)}
      >
        Kolon Sırası Değiştirme
      </Menu.Item>
      <Popover
        content={
          <>
            <Menu.Item
              className="tableMenuItem"
              key="iceren"
              icon={<img src={icerenIco} className="menuIcon" alt="İçeren" title="İçeren" />}
              onClick={() => setAramaTuru('iceren')}
            >
              İçeren
            </Menu.Item>
            <Menu.Item
              className="tableMenuItem"
              key="esit"
              icon={<img src={birebirIco} className="menuIcon" alt="Birebir" title="Birebir" />}
              onClick={() => setAramaTuru('esit')}
            >
              Birebir Eşleşme
            </Menu.Item>
          </>
        }
        placement="left"
      >
        <Menu.Item className="tableMenuItem" key="aramaTuru" icon={<SearchOutlined />}>
          Arama Turu
        </Menu.Item>
      </Popover>
      <Popover content={olumluPropList('toplam')} placement="left">
        <Menu.Item
          className="tableMenuItem"
          key="altToplam"
          icon={<img src={toplamIco} className="menuIcon" alt="Toplam" title="Toplam" />}
        >
          Alt Toplam
        </Menu.Item>
      </Popover>
      <Popover content={olumluPropList('ortalama')} placement="left">
        <Menu.Item
          className="tableMenuItem"
          key="alOrtalama"
          icon={<img src={ortalamaIco} className="menuIcon" alt="Ortalama" title="Ortalama" />}
        >
          Alt Ortalama
        </Menu.Item>
      </Popover>
      <Popover content={kolonGizleme} placement="left">
        <Menu.Item
          className="tableMenuItem"
          key="kolonGizleme"
          icon={<img src={gizleIco} className="menuIcon" alt="Gizle" title="Gizle" />}
        >
          Kolon Göster/Gizle
        </Menu.Item>
      </Popover>
        <Menu.Item
          className="tableMenuItem"
          key="toExcel"
          icon={<img src={excelIco} className="menuIcon" alt="Excele Aktar" title="Excele Aktar" />}
          onClick={() => tableToExcel('Kitap1')}
        >
          Excele Aktar
        </Menu.Item>
    </Menu>
  );

  const ara = (metin, kolonAdı) => {
    if (!metin || metin === '') {
      setfiltData(dataSource);
      return;
    }
    const kaynak = metin.toLocaleLowerCase();
    const islem = {
      iceren: (hedef) => hedef.includes(kaynak),
      esit: (hedef) => hedef === kaynak,
    };

    const aranacakCols = filtColumns.filter((col) => (kolonAdı ? col.dataIndex === kolonAdı : !col.hidden));

    let sonuc = dataSource.filter((row) => {
      let durum = false;
      let hedef;
      aranacakCols.forEach((col) => {
        hedef = String(row[col.dataIndex]);
        if (col.type === 'tarih' && row[col.dataIndex]) {
          const tarih = moment(row[col.dataIndex]);
          if (tarih.isValid()) hedef = tarih.format('DD.MM.YYYY hh:mm:ss');
        }
        hedef = hedef.toLocaleLowerCase();
        if (islem[aramaTuru](hedef)) durum = true;
      });
      return durum;
    });
    sonuc = sonuc.map((x, i) => {
      if (!x.SN) return { SN: i + 1, ...x };
      return x;
    });
    setfiltData(sonuc);
  };

  useEffect(() => {
    const tmpFiltColumns = [
      {
        title: 'SN',
        dataIndex: 'SN',
        className: 'SN',
        render: (value) => <strong>{value}</strong>,
      },
    ];
    columns.forEach((col) => tmpFiltColumns.push({ ...col }));
    setFiltColumns(kolonOzellikleri(tmpFiltColumns));
  }, [columns]);

  useEffect(() => {
    if (filtColumns.length > 0) {
      setFiltColumns(kolonOzellikleri(filtColumns));
    }
  }, [kolonSıralama]);

  useEffect(() => {
    const tmpData = dataSource.map((x, i) => {
      let item = x;
      if (!x.id && !x.ID) item = { id: Math.random().toString(), ...item };
      if (!x.SN) item = { SN: i + 1, ...item };
      return item;
    });
    setfiltData(tmpData);

    // setFiltColumns((e) =>
    //   e.map((col) => {
    //     if (col.dataIndex === 'SN') {
    //       col.render = (value, row) => <strong>{tmpData.map((x, i) => (x.id === row.id ? i + 1 : undefined))}</strong>;
    //     }
    //     return col;
    //   })
    // );
    if (dataSource && qFilter) ara(qFilter || '');
  }, [dataSource, qFilter]);

  const baslik = () => (
    <Row className="tabloBaslik">
      <div className="baslikMetni">{title}</div>
      <Space>
        {extra}
        <Input className="aramaKutusu" onPressEnter={(e) => ara(e.target.value)} addonBefore={<FilterOutlined />} />
        <Dropdown overlay={menu}>
          <Button icon={<AppstoreOutlined />} style={{ zIndex: 100 }} />
        </Dropdown>
      </Space>
    </Row>
  );

  const showTotal = (total) => <div>Toplam : {total} kayıt</div>;

  return (
    <Table
      {...props}
      id={tableId}
      ref={ref}
      dataSource={filtData}
      columns={filtColumns.filter((col) => !col.hidden)}
      title={baslik}
      className={`customAntTable ${className}`}
      summary={altlıkHazırla}
      rowClassName={rowClassName || ((e, i) => i % 2 === 1 && `tekSatir ${className}`)}
      pagination={
        // console.log(pagination)
        pagination !== false
          ? {
              showSizeChanger: true,
              showTotal,
              onShowSizeChange: (a, b) => setPageSize(b),
              ...pagination,
              pageSize,
            }
          : false
      }
    />
  );
}

Tablo.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.shape({ key: PropTypes.string, value: PropTypes.string })).isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      dataIndex: PropTypes.string,
      title: PropTypes.string,
      toplam: PropTypes.bool,
      ortalama: PropTypes.bool,
      type: PropTypes.string,
    })
  ).isRequired,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  extra: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  style: PropTypes.shape({ key: PropTypes.string, value: PropTypes.string }),
  rowKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.func]),
};

Tablo.defaultProps = {
  rowKey: (record) => record.id || record.ID,
  title: undefined,
  extra: undefined,
  style: {},
};
