# Custom_Antd_Table



## Getting started
Basic usage :
```
import Table from 'custom_antd_table';

export default function index() {
  return (
    <Table dataSource={[]} columns={[{dataIndex:'test',title:'Test'}]} />
  )
}
```
![sample](/public/Sample.jpg "sample")
Default table props :
- Based on turkish language
- Search general or colum by (equals or contains)
- Column Sort
- Summary (for number column)
- Avarage (for number column)
- Export to excel (All data or filtered)
- Color difference between rows
- Row index
- Column order change
- Hideable columns

Extra column props :

- type: PropTypes.string
		type:'tarih'  this will format column's integer value to timestamp format 'DD.MM.YYYY hh:mm:ss' automatically
- toplam: PropTypes.bool
- ortalama: PropTypes.bool,

